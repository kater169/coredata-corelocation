//
//  EKScheduledLocationManager.h
//  Tracking
//
//  Created by Martin Feige on 10.03.14.
//  Copyright (c) 2014 it-crowd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreData/CoreData.h>

@interface EKScheduledLocationManager : NSObject  <CLLocationManagerDelegate, NSFetchedResultsControllerDelegate>

- (void)getUserLocationWithInterval:(int)interval;
- (void)stopUpdatingLocation;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
