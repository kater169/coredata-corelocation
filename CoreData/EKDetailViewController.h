//
//  EKDetailViewController.h
//  CoreData
//
//  Created by Martin Feige on 13.03.14.
//  Copyright (c) 2014 it-crowd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EKDetailViewController : UITableViewController

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObject *detailItem;

@end
