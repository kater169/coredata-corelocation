//
//  EKMasterViewController.h
//  CoreData
//
//  Created by Martin Feige on 13.03.14.
//  Copyright (c) 2014 it-crowd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "EKScheduledLocationManager.h"

@interface EKMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) EKScheduledLocationManager *manager;


- (IBAction)stopTracking:(id)sender;

@end
