//
//  EKDetailViewController.m
//  CoreData
//
//  Created by Martin Feige on 13.03.14.
//  Copyright (c) 2014 it-crowd. All rights reserved.
//

#import "EKDetailViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface EKDetailViewController ()

@property (nonatomic, strong) NSArray *locations;
@property (nonatomic ,strong) NSManagedObject *point;
@property (nonatomic ,strong) NSManagedObject *run;

- (void)configureView;

@end

@implementation EKDetailViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)configureView
{
    
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        self.run = newDetailItem;
        self.point = [_detailItem valueForKeyPath:@"point"];
        NSError *err = nil;
        [self.fetchedResultsController performFetch:&err];
        self.locations = [self.fetchedResultsController fetchedObjects];
        // Update the view.
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSNotificationCenter *nc;
    nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self
           selector:@selector(syncKVO:)
               name:NSManagedObjectContextObjectsDidChangeNotification
             object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSNotificationCenter *nc;
    nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:NSManagedObjectContextObjectsDidChangeNotification object:nil];
    
}

- (void)syncKVO:(id)info
{
    NSError *err;
    [self.fetchedResultsController performFetch:&err];
    self.locations = [self.fetchedResultsController fetchedObjects];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    if (cell) {
        NSManagedObject *tpoint = [self.locations objectAtIndex:indexPath.row];
        CLLocation *ob = (CLLocation *)[tpoint valueForKey:@"location"];
        cell.textLabel.text = [NSString stringWithFormat:@"%f, %f", ob.coordinate.latitude, ob.coordinate.longitude];
        cell.detailTextLabel.text = [((NSDate *)[tpoint valueForKey:@"timestamp"]) description];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.locations count];
}

-(NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EKPoint"
                                              inManagedObjectContext:self.detailItem.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp"
                                                                   ascending:NO];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"run == %@", self.run];
    [fetchRequest setPredicate:predicate];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]
                                                             initWithFetchRequest:fetchRequest managedObjectContext:self.detailItem.managedObjectContext
                                                             sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController = aFetchedResultsController;
	NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Core data error %@, %@", error, [error userInfo]);
	    abort();
	}    
    
    return _fetchedResultsController;
}


@end
