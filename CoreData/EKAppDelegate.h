//
//  EKAppDelegate.h
//  CoreData
//
//  Created by Martin Feige on 13.03.14.
//  Copyright (c) 2014 it-crowd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;



- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
